package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"encoding/json"
	"io"
	"strconv"
)

const MAX_REQUESTS int = 5000

type Request struct {
	RequestId  string `json:"requestID"`
	UserId     int    `json:"userID"`
	CustomerId int    `json:"customerID"`
	UserAgent  string `json:"userAgent"`
	Url        string `json:"url"`
	Timestamp  int64  `json:"timestamp"`
}

type SamplerInterface interface {
	SampleRequests(pathToJsonFile string) []Request
}

type Sampler struct {
	CustomerId int	
}

func (s Sampler) SampleRequests(pathToJsonFile string) []Request {
	f, err := os.Open(pathToJsonFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	requests := make([]Request, 0)
	reader := bufio.NewReader(f)
	counter := 0
	for {
		line, _, err := reader.ReadLine()
		log.Printf("Procces the line: %s\n", line)
		if err == io.EOF {
			fmt.Printf("End of file")
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		req := Request{} 
		if err = json.Unmarshal(line, &req); err != nil {
			log.Printf("Error: %s. Skip it.", err)
			continue
		}
		if s.CustomerId == req.CustomerId {
			counter += 1
			requests = append(requests, req)
		}
		if counter == MAX_REQUESTS {
			break
		}
	}
	return requests
}

func main() {
	args := os.Args
	if len(args) != 3 {
		log.Fatal("Wrong number of arguments, should be 2")
	}
	
	log.Printf("%s", args)
	pathToJson := args[1]
	customerId, err := strconv.Atoi(args[2])
	if err != nil {
		log.Fatal("Wrong input for customerId (not a number): ", args[2])
	}
	s := Sampler{customerId}
	requests := s.SampleRequests(pathToJson)
	data, _ := json.Marshal(requests)
	log.Printf("%s", data)
}
